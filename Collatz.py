#!/usr/bin/env python3

# pylint: disable = unused-import, import-error

"""
Collatz
"""
# ----------
# Collatz.py
# ----------

from typing import IO, List, Dict

LAZY_CACHE = {}  # type: dict

# ------------
# collatz_read
# ------------


def collatz_read(s_s):
    # type: (str) -> List[int]
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a_a = s_s.split()
    return [int(a_a[0]), int(a_a[1])]

# ------------
# collatz_eval
# ------------


def collatz_eval(i, j):
    # type: (int, int) -> int
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    assert i > 0
    assert j > 0
    assert i <= 1000000
    assert j <= 1000000
    if i > j:
        i, j = j, i
    assert i <= j
    # mcl(b, e) = mcl(e / 2 + 1, e),
    # when b < e / 2 + 1; Reduce calculations by half
    half_j = j // 2 + 1
    if half_j > i:
        i = half_j
    max_len = 0
    for num in range(i, j + 1):
        if num in LAZY_CACHE:
            count_len = LAZY_CACHE[num]
        else:
            # list to keep track of countLen and intermediates
            # as loop proceeds
            other_nums = list()
            count_len, copy_num = 1, num
            while num > 1:
                other_nums.append((num, count_len - 1))
                if num % 2 == 1:
                    # If num is odd, do 2 steps: num = num * 3 + 1,
                    # num = num // 2, at a time.
                    # Simplified to num = num + (num >> 1) + 1
                    num += (num >> 1) + 1
                    if num in LAZY_CACHE:
                        count_len += LAZY_CACHE[num] + 1
                        break
                    count_len += 2
                else:
                    num = num // 2
                    if num in LAZY_CACHE:
                        count_len += LAZY_CACHE[num]
                        break
                    count_len += 1
            LAZY_CACHE[copy_num] = count_len
            # compute each intermediate num k's cycle length
            # and cache all as well as the requested num
            for (k, k_count_len) in other_nums:
                LAZY_CACHE[k] = count_len - k_count_len
        if count_len > max_len:
            max_len = count_len
    assert max_len > 0
    return max_len

# -------------
# collatz_print
# -------------


def collatz_print(w_w, i, j, v_v):
    # type: (IO[str], int, int, int) -> None
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w_w.write(str(i) + " " + str(j) + " " + str(v_v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r_r, w_w):
    # type: (IO[str], IO[str]) -> None
    """
    r a reader
    w a writer
    """
    for s_s in r_r:
        i, j = collatz_read(s_s)
        v_v = collatz_eval(i, j)
        collatz_print(w_w, i, j, v_v)
