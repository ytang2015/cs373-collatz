#!/usr/bin/env python3

# pylint: disable = import-error

"""
TestCollatz
"""
# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):

    """
    TestCollatz
    """
    # ----
    # read
    # ----

    def test_read(self):
        """
        Read Test 1
        """
        string = "1 10\n"
        i, j = collatz_read(string)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """
        Eval Test 1
        """
        val = collatz_eval(1, 10)
        self.assertEqual(val, 20)

    def test_eval_2(self):
        """
        Eval Test 2
        """
        val = collatz_eval(100, 200)
        self.assertEqual(val, 125)

    def test_eval_3(self):
        """
        Eval Test 3
        """
        val = collatz_eval(201, 210)
        self.assertEqual(val, 89)

    def test_eval_4(self):
        """
        Eval Test 4
        """
        val = collatz_eval(900, 1000)
        self.assertEqual(val, 174)

    def test_eval_5(self):
        """
        Eval Test 5
        """
        val = collatz_eval(1, 1)
        self.assertEqual(val, 1)

    def test_eval_6(self):
        """
        Eval Test 6
        """
        val = collatz_eval(3, 3)
        self.assertEqual(val, 8)

    def test_eval_7(self):
        """
        Eval Test 7
        """
        val = collatz_eval(1000, 900)
        self.assertEqual(val, 174)

    def test_eval_8(self):
        """
        Eval Test 8
        """
        val = collatz_eval(1000000, 1000000)
        self.assertEqual(val, 153)

    # -----
    # print
    # -----

    def test_print(self):
        """
        Print Test 1
        """
        writer = StringIO()
        collatz_print(writer, 1, 10, 20)
        self.assertEqual(writer.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """
        Solve tests
        """
        reader = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        writer = StringIO()
        collatz_solve(reader, writer)
        self.assertEqual(
            writer.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
